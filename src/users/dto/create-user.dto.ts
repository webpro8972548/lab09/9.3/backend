export class CreateUserDto {
  id: number;
  email: string;
  fullname: string;
  password: string;
  gender: string;
  roles: { id: number; name: string };
}
